import { createConnection } from "typeorm";
import { Food } from "../entity/Food";

export class FoodDao {
  static async getFoodByName(name: string): Promise<Food> {
    return await createConnection().then(async connection => {
      return await connection.manager
        .findOneOrFail(Food, { name: name })
        .finally(() => connection.close());
    });
  }

  static insertFood(name: string) {
    createConnection()
      .then(async connection => {
        console.log("Inserting a new food into the database");
        const food = new Food();
        food.name = name;
        await connection.manager.save(food);
        console.log("Inserted food object into the database");
        await connection.close();
      })
      .catch(error => console.log(error));
  }
}
