import { createConnection } from "typeorm";
import { User } from "../entity/User";

export class UserDao {
  static async getUserById(id: string): Promise<User> {
    return await createConnection().then(async connection => {
      return await connection.manager
        .findOneOrFail(User, id)
        .finally(() => connection.close());
    });
  }

  static insertUser(firstName: string, lastName: string) {
    createConnection()
      .then(async connection => {
        console.log("Inserting a new user into the database");
        const user = new User();
        user.firstName = firstName;
        user.lastName = lastName;
        await connection.manager.save(user);
        console.log("Inserted user into the database");
        await connection.close();
      })
      .catch(error => console.log(error));
  }
}
