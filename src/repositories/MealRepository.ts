import { EntityRepository, Repository } from "typeorm";
import { Meal } from "../entity/Meal";

@EntityRepository(Meal)
export class MealRepository extends Repository<Meal> {}
