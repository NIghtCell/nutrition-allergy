import React, { Component } from "react";
import $ from "jquery";
import { Allergy } from "../Meal/Allergy";
import { Heaviness } from "../../Heaviness";
import "./MealAdder.scss";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default class MealAdderComponent extends Component<{ saveMeal: any }> {
  state = {
    overLayIsOpen: false,
    food: [],
    allergy: new Allergy(Heaviness.None),
    startDate: new Date()
  };

  render() {
    let overlay;
    if (this.state.overLayIsOpen) {
      overlay = this.renderOverlay();
    }
    return (
      <div className="meal-adder-component">
        <button>
          <i
            className="fa fa-plus centered"
            aria-hidden="true"
            onClick={() => {
              this.openOverlay();
            }}
          ></i>
        </button>
        {overlay}
      </div>
    );
  }

  renderOverlay() {
    const food: string[] = this.state.food;
    const foodList = food.map((item, key) => {
      return (
        <li key={key}>
          {" "}
          {item} <button onClick={() => this.removeFood(item)}>x</button>{" "}
        </li>
      );
    });

    return (
      <div className="meal-adder-component-overlay">
        <div className="meal-adder-component-input">
          <div className="meal-adder-food-input">
            <input className="food-input" id="js-food-input" type="text" />
            <button
              onClick={() => {
                this.addFood();
              }}
            >
              <i
                className="fa fa-plus ml-2"
                id="js-add-food-btn"
                aria-hidden="true"
              ></i>
            </button>
          </div>
          <div className="food">
            <h5>Ingredients: </h5>
            {foodList}
          </div>
          <form>
            <label>
              Date
              <DatePicker
                selected={this.state.startDate}
                onChange={this.handleChange}
              />
            </label>
            <label>
              Allergy
              <select onChange={this.setHeaviness.bind(this)} name="allergy">
                <option>None</option>
                <option>Low</option>
                <option>Medium</option>
                <option>High</option>
              </select>
            </label>
          </form>
          <button onClick={() => this.onSaveClick()}>SAVE</button>
        </div>
      </div>
    );
  }

  handleChange = (date: Date) => {
    this.setState({
      startDate: date
    });
  };

  onSaveClick() {
    const food = this.state.food;
    if (Array.isArray(food) && food.length) {
      this.closeOverlay();
      this.props.saveMeal(
        this.state.food,
        this.state.allergy,
        this.state.startDate
      );
      this.emptyFoodList();
    } else {
      // TODO: Fehlermeldung ausgeben
      console.log("Fehlermeldung du Hirni");
    }
  }

  emptyFoodList() {
    this.setState({
      food: []
    });
  }

  openOverlay() {
    this.setState({
      overLayIsOpen: true
    });
  }

  closeOverlay() {
    this.setState({
      overLayIsOpen: false
    });
  }

  setHeaviness(event: any) {
    const heavinessText: string = event.target.value;
    let heaviness: Heaviness;
    switch (heavinessText) {
      case "High":
        heaviness = Heaviness.High;
        break;
      case "Medium":
        heaviness = Heaviness.Medium;
        break;
      case "Low":
        heaviness = Heaviness.Low;
        break;
      default:
        heaviness = Heaviness.None;
    }
    const allgery = new Allergy(heaviness);
    this.setState({
      allergy: allgery
    });
  }

  addFood() {
    const newFood: string = String($("#js-food-input").val());
    if (newFood.length > 0 && !this.isOnFoodList(newFood)) {
      $("#js-food-input").val("");
      const tempFood: string[] = [...this.state.food, newFood];
      this.setState({
        food: tempFood
      });
    }
  }

  removeFood(food: string) {
    let tempFood: string[] = [...this.state.food];
    tempFood = tempFood.filter(e => e !== food);
    this.setState({
      food: tempFood
    });
  }

  isOnFoodList(food: string) {
    const currentFood: string[] = this.state.food;
    return currentFood.includes(food);
  }
}
