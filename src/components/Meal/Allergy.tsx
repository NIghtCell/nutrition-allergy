import { Heaviness } from "../../Heaviness";

export class Allergy {
  heaviness: Heaviness;

  constructor(heaviness: Heaviness) {
    this.heaviness = heaviness;
  }
}
