import React, { Component } from "react";
import "./Meal.css";
import DateComponent from "../Date/DateComponent";
import MealAdderComponent from "../MealAdder/MealAdderComponent";

interface User {
  id: number;
  firstName: string;
  lastName: string;
}

interface Food {
  id: number;
  name: string;
}

interface Meal {
  id: number;
  date: string;
  heaviness: string;
  user: User;
  food: Food[];
}

class MealComponent extends Component {
  state = {
    date: new Date(
      new Date().getFullYear(),
      new Date().getMonth(),
      new Date().getDate()
    ),
    meals: new Array<Meal>()
  };

  async componentWillMount() {
    const date: Date = new Date(
      new Date().getFullYear(),
      new Date().getMonth(),
      new Date().getDate()
    );

    await this.getAllMeals().then(meals => {
      this.setState({
        meals: meals,
        date: date
      });
    });
  }

  async getAllMeals(): Promise<Meal[]> {
    return fetch("http://localhost:4000/nutrition-allergy/meals")
      .then(res => res.json())
      .then(res => res.map((meal: any) => this.formatMeal(meal)));
  }

  formatMeal(meal: any): Meal {
    return {
      id: meal.id,
      date: meal.date,
      heaviness: meal.heaviness,
      user: meal.user,
      food: meal.food
    };
  }

  goToNextDay = () => {
    const date: Date = this.state.date;
    date.setDate(date.getDate() + 1);
    this.setState({
      date: date
    });
  };

  goToPreviousDay = () => {
    const date: Date = this.state.date;
    date.setDate(date.getDate() - 1);
    this.setState({
      date: date
    });
  };

  // async saveMeal(food: Food[], heaviness: Heaviness, date: Date) {
  //   let user: User = new User();
  //   await UserDao.getUserById("1").then(u => {
  //     user = u;
  //   });
  //   await MealDao.insertMeal(user, food, date, heaviness);
  //   const meals = await MealDao.getAllMealsByDate(date);
  //   this.setState({
  //     date: date,
  //     meals: meals
  //   });
  // }

  render() {
    const meals = this.state.meals.map((meal, mealkey) =>
      meal.date &&
      meal.date.localeCompare(this.state.date.toJSON().slice(0, 10)) !== -1 ? (
        <div key={meal.id} className="meal">
          <div className="meal-entries">
            <div className="meal-food">
              {meal.food.map(current => (
                <li key={current.id} className="meal-food-item">
                  {current.name}
                </li>
              ))}
            </div>
            <p>
              <span>Allergy: {meal.heaviness}</span>
            </p>
          </div>
        </div>
      ) : null
    );

    return (
      <div className="container meal-component mx-auto">
        <DateComponent
          date={this.state.date}
          goToNextDay={this.goToNextDay}
          goToPreviousDay={this.goToPreviousDay}
        />
        <div className="meals">{meals}</div>
        <MealAdderComponent
          saveMeal={() => {
            console.log("save meal");
          }}
        />
      </div>
    );
  }
}

export default MealComponent;
