import React, { Component } from "react";
import "./Date.css";

class DateComponent extends Component<{
  date: any;
  goToNextDay: any;
  goToPreviousDay: any;
}> {
  convertDateString(): string {
    const date: Date = this.props.date;
    let dateString: string = "";
    dateString += date.getDate();
    dateString += " ";
    dateString += date.getMonth() + 1;
    dateString += " ";
    dateString += date.getFullYear();

    return dateString;
  }

  render() {
    return (
      <div className="date-component">
        <i
          className="fa fa-backward mr-2 date-icon"
          aria-hidden="true"
          onClick={() => {
            this.props.goToPreviousDay();
          }}
        ></i>
        <h4>{this.convertDateString()}</h4>
        <i
          className="fa fa-forward ml-2 date-icon"
          aria-hidden="true"
          onClick={() => {
            this.props.goToNextDay();
          }}
        ></i>
      </div>
    );
  }
}

export default DateComponent;
