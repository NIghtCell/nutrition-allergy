import { MealDao } from "./daos/MealDao";
import { Heaviness } from "./Heaviness";
import { User } from "./entity/User";
import { Food } from "./entity/Food";
import { UserDao } from "./daos/UserDao";
import { FoodDao } from "./daos/FoodDao";
import { Meal } from "./entity/Meal";

function testGetAll() {
  MealDao.getAll().then(meals => {
    console.log(meals);
  });
}

function testInsertUser() {
  UserDao.insertUser("Max", "Neitzel");
}

function testInsertFood() {
  FoodDao.insertFood("Banane");
}

async function testInsert() {
  let food: Food = new Food();
  await FoodDao.getFoodByName("Banane").then(f => {
    food = f;
  });
  let user: User = new User();
  await UserDao.getUserById("1").then(u => {
    user = u;
  });
  const foodArr = new Array();
  foodArr.push(food);
  MealDao.insertMeal(user, foodArr, new Date(), Heaviness.High);
}

async function testGetByDate() {
  let meals: Meal[] = new Array();
  const date = new Date(
    new Date().getFullYear(),
    new Date().getMonth(),
    new Date().getDate()
  );
  await MealDao.getAllMealsByDate(date).then(mls => {
    meals = mls;
  });
}

//testInsertUser();
//testInsertFood();
//testInsert();
//testGetAll();
testGetByDate();
