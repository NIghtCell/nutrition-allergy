export enum Heaviness {
  None = "None",
  Low = "Low",
  Medium = "Medium",
  High = "High"
}
