import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "font-awesome/css/font-awesome.min.css";
import MealComponent from "./components/Meal/MealComponent";
import MenuComponent from "./components/Menu/MenuComponent";

const App: React.FC = () => {
  return (
    <React.Fragment>
      <MenuComponent />
      <MealComponent />
    </React.Fragment>
  );
};

export default App;
