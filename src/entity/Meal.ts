import { Heaviness } from "../Heaviness";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinTable,
  ManyToMany,
  ManyToOne
} from "typeorm";
import { User } from "./User";
import { Food } from "./Food";

@Entity()
export class Meal {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: "date" })
  date: Date;

  @ManyToMany(
    type => Food,
    food => food.meals
  )
  @JoinTable()
  food: Food[];

  @Column({ type: "string" })
  heaviness: Heaviness;

  @ManyToOne(
    type => User,
    user => user.meals
  )
  user: User;
}
