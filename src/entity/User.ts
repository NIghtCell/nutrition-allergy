import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Meal } from "./Meal";

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: "string" })
  firstName: string;

  @Column({ type: "string" })
  lastName: string;

  @OneToMany(
    type => Meal,
    meal => meal.user
  )
  meals: Meal[];
}
