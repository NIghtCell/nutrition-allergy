import { Column, ManyToMany, PrimaryGeneratedColumn, Entity } from "typeorm";
import { Meal } from "./Meal";

@Entity()
export class Food {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: "string" })
  name: string;

  @ManyToMany(
    type => Meal,
    meal => meal.food
  )
  meals: Meal[];
}
